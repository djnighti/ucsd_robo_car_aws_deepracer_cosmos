from setuptools import setup

package_name = 'ucsd_robo_car_aws_deepracer_cosmos'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='root',
    maintainer_email='root@todo.todo',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'guidance_node = ucsd_robo_car_aws_deepracer_cosmos.guidance_node:main',
            'detection_node = ucsd_robo_car_aws_deepracer_cosmos.detection_node:main',
            'calibration_node = ucsd_robo_car_aws_deepracer_cosmos.calibration_node:main',
        ],
    },
)
